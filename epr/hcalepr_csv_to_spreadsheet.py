import pandas as pd
import re
from collections import defaultdict
from openpyxl.utils.cell import get_column_letter
from openpyxl.styles import Font 
''''
Aran Garcia-Bellido (June 2024)
For the latest authorlist, check people with EPR pledges for HCAL: https://icms.cern.ch/epr/showProject/216
Select All ; Export Selected to csv

Then edit the first lines in the main function to point to the csv input files that you downloaded.
To run: python3 hcalepr_csv_to_spreadsheet.py

This will produce an Excel file with one tab per institute, and the authors that have contributed EPR in each institite.
But the LatexName will be with unicode characters, no latex symbols.

In the future, this should print out for each institute who has done EPR, and then merge it with the previous official authorlist and only add new people that were not already present in the official list.

We can add others that have declared HCAL in iCMS (this includes their CMS authorship):
https://icms.cern.ch/tools/collaboration/people
You can narrow the search to "Projects: HC", and then at the bottom include all results, and click on generate xlsx file.

But it would be better to not restrict this to HC, since someone may have declared HGCAL and still do work for HCAL, so it would be better to dowload all 2.7 MB (6k+ people).
'''

def parse_user(user):  
    '''  Takes user = 'Garcia-Bellido, Aran(ROCHESTER)' and returns first name, last_name
    '''
    first_name = user.split(', ')[1].split('(')[0]
    last_name = user.split(',')[0]
    inst_code = re.search('\((.*)\)', user).group(1)
    #print(first_name,last_name,inst_code)
    return [first_name, last_name, inst_code]

def extract_epr_data_by_inst(df):
    # Add FirstName and LastName columns:
    col_first_name = []
    col_last_name = []
    for u in df["User"]: # u is something like: Kanuganti, Ankush Reddy(TENNESSEE)
        fn,ln,code = parse_user(u)
        col_first_name.append(fn)
        col_last_name.append(ln)

    df['FirstName'] = col_first_name
    df['LastName'] = col_last_name
    # Drop names that are repeated: sometimes a user will appear in two institutes.
    # For example if they have moved institutes between two years (two csv files):
    # Chen, Yi-Mu(KARLSRUHE-IEKP)
    # Chen, Yi-Mu(MARYLAND)
    # This will just keep the first occurance:
    df.drop_duplicates(subset=['LastName', 'FirstName'],inplace=True,ignore_index=True)
    #
    hcal_insts = sorted(df['Institute for Pledge'].unique()) # sorted alphabetically
    #print(hcal_insts)
    dict_users_by_inst={}
    for inst_code in hcal_insts:
        this_inst_df = df[df["Institute for Pledge"] == inst_code]
        #print(inst_code,this_inst)
        inst_users=[]
        for user in this_inst_df["User"]: # this should be unique Kanuganti, Ankush Reddy(TENNESSEE), unlike the last names or first names.
            if user not in inst_users: # add to list if not present
                # First calculate total work done, if you need to.
                this_user_df = this_inst_df[this_inst_df['User'] == user] # df with all rows for this user
                #print(this_user_df['Work Done'])
                # Use groupby to sum all numerical columns for this one user:
                summeduser = this_user_df.groupby(by='User').sum(numeric_only=True)
                #if (summeduser['Work Done'][0] > 0):
                inst_users.append(user)

        # Sort alphabetically, by surname:
        alph_inst_users=sorted(inst_users,key=str.lower)
        dict_users_by_inst[inst_code] = alph_inst_users

    hcal_epr = df[['LastName','FirstName','Institute for Pledge']].copy()
    hcal_epr.sort_values('LastName',inplace=True,key=lambda col: col.str.lower())
    print(hcal_epr)
    hcal_epr.to_csv("hcal_epr_users.csv",header=True,index=False)

    return dict_users_by_inst

def main():
    # Input files by year:
    hcal_epr_2021_file="iCMS-EPRPledgeOverviewforHCALin2021.csv"
    hcal_epr_2022_file="iCMS-EPRPledgeOverviewforHCALin2022.csv"
    hcal_epr_2023_file="iCMS-EPRPledgeOverviewforHCALin2023.csv"
    hcal_epr_2024_file="iCMS-EPRPledgeOverviewforHCALin2024.csv"
    # Need to manually edit the column name '% of work at CERN' to 'perc of work at CERN' 

    # merge all csv files into one big dataframe
    #hcal_epr_2021_file,hcal_epr_2022_file,hcal_epr_2023_file,hcal_epr_2024_file
    inputflist = [f for f in [hcal_epr_2024_file]]
    li = []
    for filename in inputflist:
        d = pd.read_csv(filename, sep=';',usecols=["Project/LV1","Activity/LV2","LV3","Task Name","Needed Work","Fraction Done","perc of work at CERN","Type of task","Comment","edit Task","User","Institute for Pledge","Work Pledged","Work Accepted","Work Done","Done/ Pledged","Status","Year","edit/update Pledge","reject Pledge","Work Already Done","PlegdeCode","FullTaskName"],index_col=False)
        li.append(d)
    df = pd.concat(li, axis=0, ignore_index=True)

    # Run the code to extract info by inst from the inclusive dataframe
    super_dict = extract_epr_data_by_inst(df)
    # Order the institites alphabetically:
    combined_dict = {}
    for elem in super_dict.keys():
        combined_dict[elem] = sorted(super_dict[elem],key=str.lower)
    
    # Dump some stats:
    hcal_insts = [i for i in combined_dict.keys()]
    hcal_users = sorted(set([u for ul in combined_dict.values() for u in ul]),key=str.lower)
    print(hcal_insts,hcal_users)
    print("N HCAL EPR insts: ", len(hcal_insts))
    print("N HCAL EPR users: ", len(hcal_users))
#    for i in hcal_users:
#        print(i)

    fname_out = "epr24.xlsx"
    writer = pd.ExcelWriter(fname_out)
    for i in combined_dict.keys():
        inst_code = i
        # Now create dataframe to write on Excel file:
        inst_fnames = [parse_user(u)[0] for u in combined_dict[i]]
        inst_lnames = [parse_user(u)[1] for u in combined_dict[i]]
        inst_authors = [''.join(i[0] for i in parse_user(u)[0].split()).upper()+'.~'+parse_user(u)[1] for u in combined_dict[i]]
        inst_orcid = [None for i in range(len(combined_dict[i]))]
        inst_note = ['' for i in range(len(combined_dict[i]))]
        inst_address = ['Latex address' if i == 0 else ' ' for i in range(len(combined_dict[i]))]
        inst_ibrep =['Please fill name and email' if i == 0 else ' ' for i in range(len(combined_dict[i]))]
        inst_df = pd.DataFrame({'FirstName':inst_fnames,'LastName':inst_lnames,'LatexName':inst_authors,
                                'ORCID':inst_orcid,
                                'Note':inst_note,
                                'Activity':['' for i in range(len(combined_dict[i]))],
                                'InstituteAddress':inst_address,
                                'IBrep':inst_ibrep,
                                'IBchecked':['' for i in range(len(combined_dict[i]))]
                                })
        #print(inst_df)
        # Write to xls file:
        inst_df.to_excel(writer, sheet_name=inst_code, index=False)
        # Make wider columns:
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('LastName')+1)].width = 15
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('LatexName')+1)].width = 20
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('ORCID')+1)].width = 20
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('Note')+1)].width = 20
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('Activity')+1)].width = 10
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('InstituteAddress')+1)].width = 20
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('IBrep')+1)].width = 20
        writer.sheets[inst_code].column_dimensions[get_column_letter(inst_df.columns.get_loc('IBchecked')+1)].width = 10

    #
    writer.close()
    print("Done: see ", fname_out)
    print("The LatexName column in the xlsx file needs to be edited with Latex symbols instead of unicode.")

if __name__ == "__main__":
    main()
