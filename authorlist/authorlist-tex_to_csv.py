import re
import pandas as pd
from openpyxl.utils.cell import get_column_letter
from openpyxl.styles import Font 
from pyphonebook import PhoneBook
# https://gitlab.cern.ch/linuxsupport/rpms/pyphonebook
from cms_institute_names import find_inst_code

'''
AGB May 2024. Used this script to create a spreadsheet for the Run 2 HCAL authorlist based on DN-18-007_authorlist_optC.tex
from https://gitlab.cern.ch/tdr/notes/DN-18-007/-/blob/master/paper/DN-18-007_authorlist_optC.tex?ref_type=heads

The spreadsheet has one worksheet for each institute (in the order they appear in the authorlist, so based on the country).
TO DO: use the abbreviated names from the CMS DB (now we use our own abbreviations) and use alphabetical order in the worksheets. But then the script that goes from xlsx to tex has to know the order of each institute for the LaTeX file...

The spreadsheet has the following columns:
'FirstName','LastName','LatexName','ORCID','Note','InstituteAddress','IBrep'

I used the CERN phonebook directory to get the first names of (most) authors. But had to resort to manually searching the ORCID info whenever there was not a match or several were found.

Note is whatever footnote is included in teh original tex file, like: "Also at CERN, Geneva, Switzerland"
'''

''''
For the latest authorlist, we could add the people with EPR pledges:
Get HCAL EPR pledges: https://icms.cern.ch/epr/showProject/216
Select All ; Export Selected
'''

def find_first_name(author):
    '''On Alma9: sudo dnf install pyphonebook # requires ldap3
        https://gitlab.cern.ch/linuxsupport/rpms/pyphonebook
        Author is a string like: E.J.~Tonelli~Manganote
        
        We should put this in a try statement in case we don't have it where we run
    '''
    #pyphonebook --surname Pedro --group UCM
    #phonebook.multi_field_search({'first_name': 'John', 'group': 'BI'})
    surname = author.split('~',maxsplit=1)[1]
    initial = author.split('~',maxsplit=1)[0]
    # remove latex accents from surname:
    surname = surname.replace('\\"{','').replace("\\'{","").replace('~',' ').replace('}','')
    surname = surname.replace('\\dag','') # for deceased
    #
    firstname = initial
    cernpb = PhoneBook()
    query = cernpb.multi_field_search({'last_name': surname, 'group': 'UCM'})
    if len(query) == 0:
        print('Warning: no entry for ', initial, surname)
        # Try removing the UCM group requirement:
        newq = cernpb.multi_field_search({'last_name': surname})
        if len(newq)>0:
            print("Possible people not in UCM:", [q.first_name for q in newq])
        return firstname
    possible = [q.first_name for q in query]
    print("phonebook: ", initial, surname, possible)
    nmatch = 0
    for p in possible:
        if initial[0] == p[0][0]: # we have the first match
            firstname = p[0]
            nmatch = nmatch+1
    if nmatch > 1:
        print('More than one match for ', initial, surname, ' possible firstnames = ', possible)
    #if firstname == initial:
    #    print('find_first_name: WARNING could not find ', initial, surname )

    return firstname    

def main():
    tex_file = "DN-18-007_authorlist_optC.tex"
    writer = pd.ExcelWriter("HCAL_Run2_authors.xlsx")

    with open(tex_file, "r") as file:
        content = file.read().replace("\n", "")
        # now we have all in one line.

    content = content.replace('\cmsinstitute{','\n\cmsinstitute{')
    #content = content.replace('{\\tolerance=6000','\n')
    content = content.replace('\par}','')
    content = content.replace('\\\\','\n') # make a line for each note

    lines = content.split('\n')
    lines = lines[1:] # remove first part of latex document
    hcal_institutes=[]
    country_dict_by_inst={}
    author_dict_by_inst={}
    orcid_dict_by_author={}
    note_dict_by_author={}
    for line in lines:
        if line.startswith('%') or line.startswith('\\newcommand'):
            continue
        if "cmsinstitute" in line and '6000' in line:
            inst=line.split("cmsinstitute{")[1].split(',')[0].replace('~','')
            if inst == 'UniversityofCalifornia':
                inst += line.split(',')[1].replace('~','')
            elif "Authors" in inst: # These are the CERN-affiliated by MOU (option C)
                inst = 'MOUAffiliated'
            address = re.search('{(.*?)}', line).group() # non-greedy match, stops at the first }
            address = address.replace('{','').replace('}','')
            #inst = simplify_inst_name(inst)
            instcode = find_inst_code(inst)
            print(instcode)
            #print(inst_address)

            #hcal_institutes.append(instcode)

            country = line.split("}{\\tolerance=6000")[0].split(',')[-1] # last comma
            country = country.split("\cmsAuthorMark")[0]
            country = country.replace('~','').lstrip()
            country_dict_by_inst[instcode]=country

            authors = line.split('}{\\tolerance=6000')[1]
            authors = authors.split(',')
            inst_authors = [] ; inst_orcid = []; inst_note = []
            for a in authors:
                note = ''
                if 'cmsorcid' in a:
                    author = a.split('\cmsorcid')[0].lstrip()
                    orcid = a.split('\cmsorcid')[1].replace('{','').replace('}','')
                    orcid = orcid[:19] # ORCIDs should only have 19 characters
                    # the last entry for A.~Zhokin matches spurious stuff after the ORCID
                else:
                    author = a.lstrip()
                    orcid = ''
                if 'AuthorMark' in author:
                    mark = re.findall('\d+',author)[0]
                    note = re.findall('\$\^\{'+str(mark)+'\}\$(.*)',content)[0]
                    # returns everything after $^{14}$
                    author = author.split('\cmsAuthorMark')[0]

                print(' ', author, orcid, note)

                inst_authors.append(author)
                inst_orcid.append(orcid)
                inst_note.append(note)
                orcid_dict_by_author[author]=orcid
                note_dict_by_author[author]=note

            print(len(inst_authors))
            # Back to inst level:
            author_dict_by_inst[instcode]=inst_authors
            #inst_fnames = [a.split('~',maxsplit=1)[0] for a in inst_authors]
            inst_fnames = [find_first_name(a) for a in inst_authors]
            inst_lnames = [a.split('~',maxsplit=1)[1].replace('\\"{','').replace("\\'{","").replace('~',' ').replace('}','') for a in inst_authors]
            #rint(inst_lnames)
            inst_address = [address if i == 0 else ' ' for i in range(len(inst_authors))]
            inst_ibrep =['Please fill name and email' if i == 0 else ' ' for i in range(len(inst_authors))]
            #inst_ibrep = ['caca']*len(inst_authors) #; inst_ibrep[0] = 'Please fill name and email'
            inst_df = pd.DataFrame({'FirstName':inst_fnames,'LastName':inst_lnames,'LatexName':inst_authors,
                                    'ORCID':inst_orcid,
                                    'Note':inst_note,
                                    'InstituteAddress':inst_address,
                                    'IBrep':inst_ibrep
                                    })
            # Write to xls file:
            inst_df.to_excel(writer, sheet_name=instcode, index=False)
            # Make wider columns:
            writer.sheets[instcode].column_dimensions[get_column_letter(inst_df.columns.get_loc('LastName')+1)].width = 15
            writer.sheets[instcode].column_dimensions[get_column_letter(inst_df.columns.get_loc('LatexName')+1)].width = 20
            writer.sheets[instcode].column_dimensions[get_column_letter(inst_df.columns.get_loc('ORCID')+1)].width = 20
            writer.sheets[instcode].column_dimensions[get_column_letter(inst_df.columns.get_loc('Note')+1)].width = 20
            writer.sheets[instcode].column_dimensions[get_column_letter(inst_df.columns.get_loc('InstituteAddress')+1)].width = 20
            writer.sheets[instcode].column_dimensions[get_column_letter(inst_df.columns.get_loc('IBrep')+1)].width = 20
            

    writer.close()

    #print(author_dict_by_inst)
    #print(orcid_dict_by_author)
    #print(note_dict_by_author)


if __name__ == "__main__":
    main()
