import pandas as pd
import sys
from string import ascii_lowercase
'''
Aran Garcia-Bellido (Dic 2024)
Download the HCAL authorlist GoogleSheets spreadsheet with authors for each institute, and convert that to the authorlist tex file to be added to the paper. This script writes out a big block with all the authors, and their affiliations as number-footnnotes, then notes with marks a-z, aa,bb,cc-zz. The order is by institute, starting with Armenia and finishing with USA, then for each institute each author is listed alphabetically.

To run: python3 authorlist-spreadsheet_to_tex_onegroup.py HCAL_Run3_authors.xlsx > myauthorlist.tex

To get a header with each institute and then its authors, use authorlist-spreadsheet_to_tex_by_inst.py
'''

#ascii_lowercase #'abcdefghijklmnopqrstuvwxyz'
#list(ascii_lowercase) # ['a', 'b', 'c', 'd', 'e', ...]
letter_counter = list(ascii_lowercase) + [letter1+letter2 for letter1 in ascii_lowercase for letter2 in ascii_lowercase if letter1==letter2]
#letter_counter=[a,...,z,aa,bb,...,zz] # this removes the ab,...,bc combinations...

infile = sys.argv[1] # HCAL_Run3_authors.xlsx

# Read an xlsx file with many sheets and combine all sheets into one DataFrame
# while adding a column called InstCode that says which sheet the rows come from
workbook = pd.ExcelFile(infile, engine='openpyxl')
sheets = workbook.sheet_names 
comb_df = pd.concat([pd.read_excel(workbook, sheet_name=s).assign(InstCode=s) for s in sheets],ignore_index=True).dropna(thresh=3)
# If any row has more than 3 NaN entries, it gets dropped.
#print(comb_df)

# First, check for duplicates:
if (comb_df['LatexName'].duplicated().any()):
    print(comb_df[comb_df['LatexName'].duplicated() == True])
    print("WARNING: spreadsheet has Duplicate entries!!! Fix it!!!")

# AGB: TODO: double check that the surnames are sorted alphabetically within each inst

# The next line would sort ALL names alphabetically, but the order should be alphabetical within each Institute, and the Institutes in alphabetical order.
#sorted_comb_df = comb_df.sort_values(by=["LastName","FirstName"], ignore_index = True, key=lambda col: col.str.lower())
#print(sorted_comb_df)

# Now get a dictionary for the address for each InstCode (easier to do by reading each sheet separately)
df = pd.read_excel(infile,sheet_name=None,header=0,engine='openpyxl') # dictionary with key the sheet name (InstCode)
inst_codes = df.keys() # for example FERMILAB
# Creates dict in one line, getting the address from the first row of each sheet for the column InstituteAddress
dict_instcode_instaddress = {i:df[i]['InstituteAddress'][0] for i in inst_codes}
#print(dict_instcode_instaddress)

# now assign a number to each institute which will be the index to assign in each author. We do it by alphabetical order (like in CMS) of the country and then the InstCode. Not in order of appearance for each author.
dict_instcode_number = {i:num+1 for num,i in enumerate(inst_codes)}
#print(dict_instcode_number)

dict_notes = {} # dict with note "Now at University..." as key and 'a', 'b',...,'aa','bb',... as values, in the order they appear in the spreadsheet
note_idx = 0 # counts notes
for j,a in enumerate(comb_df['LatexName']): #sorted_comb_df['LatexName']
    # Sometimes there will be nan values in the downloaded spreadsheet, not visible in GDocs.
    # This typically happens when we add or remove rows online.
    if pd.isna(a): # if nan, then skip to next
        continue
    a = a.strip() # remove empty spaces
    instcode = comb_df['InstCode'][j]
    inum = dict_instcode_number[instcode]
    mark = '' # something like 'aa' from the note.
    if pd.notna(comb_df['Note'][j]):
        note = comb_df['Note'][j]
        if note == 'deceased':
            note = 'Deceased'
        if note == 'emeritus':
            note = 'Emeritus'
        if note not in dict_notes.keys() and note != 'Deceased':
            mark = letter_counter[note_idx]
            dict_notes[note] = mark
            note_idx += 1
        elif note == 'Deceased':
            mark = '\\dag'
        else: # this note is already in the dictionary
            mark = dict_notes[note] # Something like 'aa' or '\\dag'

    orcid = ''
    if pd.notna(comb_df['ORCID'][j]):
        orcid = '\\cmsorcid{'+str(comb_df['ORCID'][j])+'}'

    # We want to get something like:
    # \author{R.G.~Kellogg\cmsorcid{0000-0001-9235-521X}}$^{25,d}$,
    # sometimes there will be a note, sometimes not.
    text = '\\author{'+a+orcid+'}$^{'+str(inum)
    if mark != '': # \author{A.~Stepennov}$^{34,hh}$,
        text += ','+mark+'}$, '
    else: # \author{A.~Oskin}$^{34}$,
        text += '}$, '
    if j == len(comb_df['LatexName'])-1:
        text = text.replace(', ','') # remove last comma

    print(text) #\author{A.~Stepennov\orcidlink{0000-0001-7747-6582}}$^{46,p}$,

print('\n% The CMS-HCAL Collaboration\n')
print('\\vspace{4ex}\n')

for i,v in enumerate(dict_instcode_instaddress.values()):
    print('$^{'+str(i+1)+'}$'+str(v)+'\\\\') #$^{1}$Yerevan Physics Institute, Yerevan, Armenia\\
print('\n\\begin{flushleft}')
print('$^{*}$No longer in CMS HCAL Collaboration\\\\')
print('$\\dag$:~Deceased\\\\')
# Check if there are any entries with this, if not comment out the line:
for k,v in dict_notes.items():
    print('$^{'+v+'}$'+k+'\\\\') #$^{b}$Also at Yerevan State University, Yerevan, Armenia\\
print('\\end{flushleft}')

