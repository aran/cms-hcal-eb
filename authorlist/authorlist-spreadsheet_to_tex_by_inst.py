import sys
import pandas as pd
'''
Aran Garcia-Bellido (June 2024)
Download the HCAL authorlist GoogleSheets spreadsheet with authors for each institute, and convert that to the authorlist tex file to be added to the paper. This script writes out each institute name in the same order as in CMS (alphabetically by country, so Armenia is first, USA last) and then the authors alphabetically.

To run: python3 authorlist-spreadsheet_to_tex_by_inst.py HCAL_Run3_authors.xlsx > myauthorlist.tex

To get all authors in alphabetical order and the institute as a footnote, use authorlist-spreadsheet_to_tex_onegroup.py

Note: it may happen that you get a NaN in LatexName if someone removes rows, that gives
an error: TypeError: can only concatenate str (not "float") to str in authors += a
To fix it, you can print(d['LatexName']) inside the loop over inst_codes and find which tab is at fault. And then remove a couple of rows below the last entry on the sheet. This has now been fixed by checking for NaN and skipping.
'''

# Read all the worksheets in the file:
infile = sys.argv[1]
df = pd.read_excel(infile,sheet_name=None,header=0,engine='openpyxl')
# df is a dictionary with each key the name of the worksheet and the value is a dataframe with all columns in that worksheet.
inst_codes = df.keys()
#for i in inst_codes:
#    print(i)

# Check for duplicates:
comb_df = pd.concat([df[s] for s in inst_codes],ignore_index=True).dropna(thresh=3)
if (comb_df['LatexName'].duplicated().any()):
    print(comb_df[comb_df['LatexName'].duplicated() == True])
    print("WARNING: spreadsheet has Duplicate entries!!! Fix it!!!")

preamble = '''
\\parindent 0pt
\\parskip 4pt
\\newcommand{\\cmsinstitute}[1]{\\par\\pagebreak[3]\\bfseries #1 \\mdseries\\\\[0pt]}
\\newcommand{\\cmsorcid}[1]{\\href{https://orcid.org/#1}{\\hspace*{0.1em}\\raisebox{-0.45ex}{\\includegraphics[width=1em]{ORCIDiD_iconvector.pdf}}}}
\\makeatletter
\\newcommand{\\cmsAuthorMark}[1]{\\hbox{\\@textsuperscript{\\normalfont#1}}}
\\makeatother
\\newskip{\\cmsinstskip} \\cmsinstskip=4pt

%\\cmsinstitute{The CMS HCAL Collaboration}
'''
print(preamble)

dict_notes = {} # dict with note as key and '$^{1}$', '$^{2}$'... as values, in order they appear in the spreadsheet
mark = 1
for i in inst_codes:
    d=df[i] # read each worksheet as a dataframe
    #print(i) #print(d)
    address = d['InstituteAddress'][0] # first row
    print('\\cmsinstitute{'+address+'}')
    print('{\\tolerance=6000')
    # now loop over authors:
    authors = ''
    #print(d['LatexName'])
    # Create subset and sort alphabetically by 'LastName':
    sorted_d = d[['FirstName', 'LastName','LatexName','ORCID','Note']].sort_values(by=['LastName','FirstName'],key=lambda col: col.str.lower(),ignore_index=True)
    # the lower case is needed for 'de~Barbaro', otherwise it goes last.
    # the ignore_index ensures the new sorted_d is in correct order 0,1,...,n-1; not the original order
    # print(sorted_d)
    for j,a in enumerate(sorted_d['LatexName']):
        if pd.isna(a): # if nan, then skip to next
            continue
        authors += a
        if pd.notna(sorted_d['Note'][j]):
            note = sorted_d['Note'][j]
            if note == 'deceased':
                note = 'Deceased'
            if note == 'emeritus':
                note = 'Emeritus'
            if note not in dict_notes.keys() and note != 'Deceased':
                dict_notes[note] = '$^{'+str(mark)+'}$'
                authors += '\\cmsAuthorMark'+'{'+str(mark)+'}'
                mark += 1
            elif note == 'Deceased':
                authors += '\\dag'
            else: # this note is already in the dictionary
                mymark = dict_notes[note] # Something like '$^{12}$'
                nummark = int(mymark[mymark.find("{")+1:mymark.find("}")]) # get the numbers between the brackets
                authors += '\\cmsAuthorMark{'+str(nummark)+'}'

        if pd.notna(sorted_d['ORCID'][j]):
            orcid='\\cmsorcid{'+str(sorted_d['ORCID'][j])+'}'
            authors += orcid
        if j != len(sorted_d['LatexName'])-1: # this is not the last one
            authors += ', '
    print(authors)
    print('\\par}','\n')
# Done with all the institutes
print('\\vskip\\cmsinstskip','\n')
print('$\\dag$:~Deceased\\\\')
# Check if there are any entries with this, if not comment out the line:
print('$^{*}$No longer in CMS HCAL Collaboration\\\\')
# Print the notes:
for k,v in dict_notes.items():
    print(v+k+'\\\\') # no space between the value and the key and the \\
