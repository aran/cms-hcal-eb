## cms-hcal-eb

Tools to manage the authorlist for the CMS HCAL collaboration. 

## Authorlist (Run 3)

The main Google spreadsheet needs to be upddated by the IB representatives, and once that's done, you can download it and run the scripts in the folder authorlist/ 

- [ ] [authorlist/authorlist-spreadsheet_to_tex_by_inst.py](https://gitlab.cern.ch/aran/cms-hcal-eb/-/blob/master/authorlist/authorlist-spreadsheet_to_tex_by_inst.py) 
This will create the default format for the authorlist with a header for each institute and the authors in alphabetical order for each institute. 

- [ ] [authorlist/authorlist-spreadsheet_to_tex_onegroup.py](https://gitlab.cern.ch/aran/cms-hcal-eb/-/blob/master/authorlist/authorlist-spreadsheet_to_tex_onegroup.py) 
This will create the authorlist with all authors one after the other in a big block, with a superscript number for their affiliation and a superscript letter \[a-z,aa,bb,cc-zz\] for notes. 

## EPR contributions

You can check them here: [iCMS EPR](https://icms.cern.ch/epr/showProject/216)
To download: "Select All" > "Export Selected to csv".
You can then run epr/hcalepr_csv_to_spreadsheet.py to create a template of the authorlist spreadsheet.  

## CMS institutes

You can find the whole list with short-names and full addresses here: 
[iCMS Institutes](https://cms.cern.ch/iCMS/jsp/secr/stats/customizeInstSearch.jsp?InstIden=&OutFORM=FULL)

## Latest Run 2 authorlist 

The latest Run 2 authorlist can be found here: [DN-18/007 in gitlab](https://gitlab.cern.ch/tdr/notes/DN-18-007/-/blob/master/paper/DN-18-007_authorlist_optC.tex?ref_type=heads) which corresponds to this paper [arXiv:2207.11960](https://arxiv.org/abs/2207.11960)